extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(PoolIntArray) var coursePoints
# x = x position
# y = turn speed
# z = distance
var currentPoint = 0
var lap = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	position.x = 515

#Called every frame. 'delta' is the elapsed time since the previous frame.
func progress(delta):
	if position.x - coursePoints[currentPoint] > 5:
		position.x -= 100 * delta
	elif position.x - coursePoints[currentPoint] < -5:
		position.x += 100 * delta
	else:
		advancePoint()

func advancePoint():
	currentPoint += 1
	if currentPoint >= coursePoints.size():
		currentPoint = 0
		lap += 1
