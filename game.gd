extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var speed = 0
const top_speed = 50
const acceleration = 5
const decceleration = 10
const turn_speed = 1500

var collided = true

var courseProgress = 0

signal progress
signal advance

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("progress", $Area2D, "progress")
	connect("advance", $Area2D, "advancePoint")

func _process(delta):
	if Input.is_action_pressed("ui_up"):
		if !collided:
			if $Car/Crash.playing:
				$Car/Crash.stop()
			if !$Car/Accelerate.playing:
				$Car/Idle.stop()
				$Car/Accelerate.play()
			elif speed > 20:
				if !$Car/Drive.playing:
					$Car/Accelerate.stop()
					$Car/Drive.play()
			if speed <= top_speed:
				speed += acceleration * delta
			
			if $Area2D.currentPoint % 2 == 0:
				courseProgress += speed * delta
				if courseProgress >= $Area2D.coursePoints[$Area2D.currentPoint]:
					emit_signal("advance")
					courseProgress = 0
			else:
				emit_signal("progress", delta)
			print($Area2D.position.x)
		elif !$Car/Crash.playing:
			$Car/Accelerate.stop()
			$Car/Drive.stop()
			$Car/Crash.play()
			speed = 0
	elif $Car/Crash.playing:
		$Car/Crash.stop()
	elif !$Car/Idle.playing:
		$Car/Accelerate.stop()
		$Car/Drive.stop()
		$Car/Idle.play()
	elif speed > 0:
		speed -= decceleration * delta
		if speed < 0:
			speed = 0
	#print(speed)
	
	if Input.is_action_pressed("ui_left"):
		if !$Turn.playing:
			$Turn.play()
		if $Turn.position.x >= 0:
			$Turn.position.x -= turn_speed * delta
		$Car.position.x -= turn_speed * delta
	elif Input.is_action_pressed("ui_right"):
		if $Turn.position.x <= 1024:
			$Turn.position.x += turn_speed * delta
		if !$Turn.playing:
			$Turn.play()
		$Car.position.x += turn_speed * delta
	elif $Turn.playing:
		$Turn.stop()
	elif $Turn.position.x < 512:
		$Turn.position.x += (turn_speed * 1.5) * delta
		if $Turn.position.x > 512:
			$Turn.position.x = 512
	elif $Turn.position.x > 512:
		$Turn.position.x -= (turn_speed * 1.5) * delta
		if $Turn.position.x < 512:
			$Turn.position.x = 512
	print(courseProgress)

func advanceCourse():
	pass

func _on_Area2D_body_entered(body):
	collided = false

func _on_Area2D_body_exited(body):
	collided = true
